#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(chri_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

/**
 * basic_boot is a simple environment for integration tests which doesn't call and initialization
 * routintes
 **/

use core::panic::PanicInfo;
use chri_os::println;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();

    loop{}
}

fn test_runner(tests: &[&dyn Fn()]) {
    unimplemented!();
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    chri_os::test_panic_handler(info)
}


#[test_case]
fn test_println() {
    println!("test_println output");
}
