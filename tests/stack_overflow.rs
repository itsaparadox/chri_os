#![no_std]
#![no_main]
#![feature(abi_x86_interrupt)]

/**
 * stack_overflow is an environment for testing the handling of double faults caused by stack
 * overflows
 */

use core::panic::PanicInfo;
use chri_os::{exit_qemu, QemuExitCode, serial_print, serial_println};
use lazy_static::lazy_static;
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};


lazy_static! {
    static ref TEST_IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        unsafe {
            idt.double_fault
                .set_handler_fn(test_double_fault_handler)
                .set_stack_index(chri_os::gdt::DOUBLE_FAULT_IST_INDEX);
        }
        idt
    };
}

pub fn init_test_idt() {
    TEST_IDT.load();
}

// custom double fault handler which exits the vm with a success code
extern "x86-interrupt" fn test_double_fault_handler(
    _stack_frame: &mut InterruptStackFrame,
    _error_code: u64,
) -> ! {
    serial_println!("[ok]");
    exit_qemu(QemuExitCode::Success);
    loop {}
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    serial_print!("Stack_overflow::stack_overflow...\t");

    chri_os::gdt::init();
    init_test_idt();

    stack_overflow();

    panic!("Execution Continued after Stack Overflow");
}

#[allow(unconditional_recursion)]
fn stack_overflow() {
    stack_overflow();
    //prevent tail recursion optimization
    volatile::Volatile::new(0).read();
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    chri_os::test_panic_handler(info);
}
