#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(chri_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;
use chri_os::println;


// ***********************
// Entry Point for Program
// ***********************
#[no_mangle]
pub extern "C" fn _start() -> ! {
    println!("Hello World{}", "!");

    chri_os::init();

    //trigger a page fault
    unsafe {
        *(0xdeadbeef as *mut u64) = 42;
    }

    #[cfg(test)]
    test_main();

    println!("It did not crash!");
    loop{}
}

// ***************************************
// panic handler called when running tests
// ***************************************
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    loop {}
}

// ******************************************
// panic handler called when running normally
// ******************************************
#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    chri_os::test_panic_handler(info)
}
